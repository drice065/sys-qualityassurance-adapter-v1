package com.mulesoft.transformer;

import org.mule.api.MuleMessage;
import org.mule.api.context.MuleContextAware;
import org.mule.api.transformer.TransformerException;
import org.mule.transport.jms.transformers.JMSMessageToObject;

import javax.jms.Message;
import java.util.Map;


@SuppressWarnings("unused")
public class CustomJMSMessageToObject extends JMSMessageToObject{

    @Override
    public Object transformMessage(MuleMessage message, String outputEncoding) throws TransformerException {
        //get a hold of the shared map
        Map<String, Message> messageMap = muleContext.getRegistry().lookupObject("messageMap");

        //put the AMQ message in the map
        messageMap.put(message.getUniqueId(), (Message) message.getPayload());

        return super.transformMessage(message, outputEncoding);
    }
}

package com.mulesoft.component;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import javax.jms.Message;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: germanrsolis
 * Date: 9/25/13
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 */
@SuppressWarnings("unused")
public class AckErrorComponent implements Callable {

    @Override
    public Object onCall(MuleEventContext eventContext) throws Exception {
        //get the AMQ Message from the map
        Message message = (Message) eventContext.getMessage().getPayload();

        //ack the message
        message.acknowledge();

        return eventContext;
    }
}

package com.mulesoft.component;

import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

import javax.jms.Message;
import java.util.Map;

/**
 * Created with IntelliJ IDEA.
 * User: germanrsolis
 * Date: 9/25/13
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class AckComponent implements Callable {

    private Map<String, Message> messageMap;

    public Map<String, Message> getMessageMap() {
        return messageMap;
    }

    public void setMessageMap(Map<String, Message> messageMap) {
        this.messageMap = messageMap;
    }

    @Override
    public Object onCall(MuleEventContext eventContext) throws Exception {
        //get the AMQ Message from the map
        Message message = messageMap.get(eventContext.getMessage().getUniqueId());

        //ack the message
        message.acknowledge();

        return eventContext;
    }
}

import groovy.util.slurpersupport.GPathResult;

Map<String,String> lookups = [
	BLDV: 'VENOUS'
]

XmlSlurper slurper = new XmlSlurper()
GPathResult xml = slurper.parseText(payload)

def Xml = slurper.parseText(payload) String result = lookups[xml.'ORU_R01.PATIENT_RESULT'.'ORU_R01.ORDER_OBSERVATION'.'OBR.15'.'SPS.3'.text()]

return result
